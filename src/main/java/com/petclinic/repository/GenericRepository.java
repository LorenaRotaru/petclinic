package com.petclinic.repository;

import com.petclinic.util.HibernateUtil;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class GenericRepository<X> {
    private SessionFactory sessionFactory = HibernateUtil.getInstance();


    public void save(X object) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.save(object);
        transaction.commit();
        session.close();
    }
}
