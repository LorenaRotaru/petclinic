package com.petclinic.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Appointment {
    @Id
    @GeneratedValue
    private Integer appointmentId;

    private LocalDateTime dateTime;
    private Boolean isCancelled;

    @ManyToOne
    @JoinColumn
    private Pet pet;

    @ManyToOne
    @JoinColumn
    private Vet vet;
}
